(put 'downcase-region 'disabled nil)

(find-file "~/.emacs.d/personal/hakyll/hakyll.el")


(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)

(fset 'git-rm
   [?\C-x ?b ?* ?g ?i ?t ?- ?r ?m return ?y backspace ?\C-y ?\C-p ?\C-s ?t ?e ?d ?: ?\C-m escape ?< ?\C-x ?r ?k ?\C-x ?r ?t ?g ?i ?t ?  ?r ?m return ?\C-n ?\C-w ?\C-y ?\C-  escape ?< ?\C-w ?\C-x ?k return])
(put 'upcase-region 'disabled nil)


(fset 'gimme-digits
  [?\C-x ?b ?* ?d ?i ?g ?i ?t ?s ?* return ?\C-x ?k return ?\C-x ?b
?* ?d ?i ?g ?i ?t ?s ?* return ?\C-x ?i ?~ ?/ ?d
?i ?g ?i ?t ?s ?. ?t ?x ?t return])

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(add-to-list 'load-path "~/.emacs.d/personal/hakyll/")
(add-to-list 'load-path "~/.emacs.d/personal/markdown-mode/")
(load "hakyll.el")
(load "hack.rn.el")
(load "~/.emacs.d/personal/reddit")




(global-set-key (kbd "C-c j") 'hakyll-new-post)
(global-set-key (kbd "C-c d") 'hakyll-new-dream-post)
(global-set-key (kbd "C-c n") 'hakyll-new-japanese-post)
(global-set-key (kbd "C-c r") 'ruby-tag-create-region)
(global-set-key (kbd "C-c 3") 'markdown-timestamp-short)
(global-set-key (kbd "C-c #") 'markdown-timestamp-full)
(global-set-key (kbd "C-c t") 'markdown-timestamp-short)
(global-set-key (kbd "C-c T") 'markdown-timestamp-full)

(global-set-key (kbd "C-c l") 'eval-buffer)

;(fset 'journal-go-to-directory
;   [?\C-x ?\C-f ?~ ?/ ?j ?o ?u ?r ?n ?a ?l ?/ ?2 ?0 ?1 ?6 ?/ ?0 ?3 return escape ?>])
;(global-set-key (kbd "C-c c") 'journal-go-to-directory)

(fset 'create-thumbs-with-b\.robnugen\.com-image-url-on-single-line
   [?\C-e ?\C-r ?/ return ?\C-f ?\C-k ?\C-y ?\C-x ?b ?* ?j ?h ?h ?g ?a ?a return ?\C-y backspace backspace backspace backspace ?\C-a escape ?x ?r ?e ?p ?l tab ?s ?t tab return ?_ return ?  return ?\C-a ?\C-k ?\C-x ?k return ?\C-a ?\[ ?! ?\[ ?\C-y ?\] ?\( ?\C-s ?/ ?/ return ?\C-b ?\C-b ?\C-w ?\C-f ?\C-f ?\C-b ?\C-b ?\C-k ?\) ?\] ?\( ?\C-y ?\) ?\C-a return ?\C-p ?\C-y ?\C-r ?/ return ?/ ?t ?h ?u ?m ?b ?s ?\C-a ?\C-k ?\C-d ?\C-s ?\] ?\( ?\) ?\] return left left ?\C-y ?\C-a ?\C-n])
(global-set-key (kbd "C-c !") 'create-thumbs-with-b\.robnugen\.com-image-url-on-single-line)

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch)
      (goto-char (point-max))
      (eval-print-last-sexp))))

(el-get 'sync)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(dired-recursive-copies (quote always))
 '(line-move-visual nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
